<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Content-type: application/json; charset=UTF-8');
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Origin, X-Requested-With, Accept");

// includes
include_once 'config/Database.php';
include_once 'models/User.php';
include_once 'models/Session.php';

$database = new Database();
$db = $database->getConnection();

$user    = new User($db);
$session = new Session($db);

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$password = $_POST['password'];

if (
    empty($firstname) OR
    empty($lastname) OR
    empty($email) OR
    empty($password)
    ) {
    http_response_code(400);
    echo json_encode([
        'status' => false,
        'message' => 'All fields must be filled'
    ]);
    die();
}

// данные формы
$user->firstname = $firstname;
$user->lastname = $lastname;
$user->email = $email;
$user->password = $password;

// очищаем данные от html тегов
$user->clearData();

// если такой email занят
$email_exists = $user->exist_email();

if ($email_exists) {
    http_response_code(400);
    echo json_encode([
        'status' => false,
        'message' => 'this email already exists'
    ]);
    die();
}

// если email свободен
$created = $user->create();

if ($created) {
    http_response_code(201);

    $session->generate();
    
    setcookie('session_id', $session->hash, 0, '/', 'lib-gen.client.com', false, true);
    echo json_encode([
        'status' => true,
        'message' => 'User created',
        'data' => [
            'id' => $user->id,
            'name' => $user->name,
            'surname' => $user->surname,
            'email' => $user->email,
            'created' => $user->created
        ]
    ]);
    die();

}

// возврат ответа если что-то не сработало
http_response_code(400);
echo json_encode([
    'status' => false,
    'message' => 'Oops some error'
]);
die();