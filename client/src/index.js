import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import common_ru from './translations/ru/common.json';
import common_en from './translations/en/common.json';
import {
    RecoilRoot,
    atom,
    selector,
    useRecoilState,
    useRecoilValue
} from 'recoil';
import Test from './test.js';

i18next.init({
    interpolation: { escapeValue: false },
    lng: localStorage.getItem('lang') || 'ru',
    resources: {
        ru: {
            common: common_ru
        },
        en: {
            common: common_en
        }
    }
});

const textState = atom({
    key: 'textState',
    default: '3131',
});

ReactDOM.render(
    <React.StrictMode>
        <RecoilRoot>
            <I18nextProvider i18n={i18next}>
                <Test textState={textState} />
            </I18nextProvider>
        </RecoilRoot>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
