import React, { useEffect } from 'react'
import './signUp.sass';

export default function Registration() {

    let form;
    let inputs;

    useEffect(() => {
        form = document.querySelector('.sign-up-form');
        inputs = document.querySelectorAll('.sign-up-form input');
    }, []);

    const sendData = async e => {
        e.preventDefault();
        // массив с заполненными инпутами
        let verified_arr = [];
        // цикл проверки всех инпутов
        for (let item of inputs) {
            item.value ? verified_arr.push(item) : item.classList.add('red');
        }
        // если все инпуты проверены
        if (inputs.length === verified_arr.length) {
            // форма с данными
            const formData = new FormData(form);
            // запрос
<<<<<<< HEAD
            const request = await fetch('http://127.0.40.40/api/create_user.php', {
                method: 'POST',
                body: formData,
            }).catch(err => console.log('Ошибка: ' + err));
            // то что придёт с бэка делаем в объект
            const response = await request.text();
=======
            const request = await fetch('http://lib-generator/api/create_user.php', {
                method: 'POST',
                body: formData
            }).catch(err => console.log('Ошибка: ' + err));
            // то что придёт с бэка делаем в объект
            const response = await request.text();

>>>>>>> 8e96d1c5c894d7fe779d96e60dbb770aa2add1a9
            console.log('send', response)
        }
    }

    const validate = e => {
        if (e.target.value) {
            e.target.classList.remove('green', 'red');
            e.target.classList.add('green');
        } else {
            e.target.classList.remove('green', 'red');
        }
    }

    return (
        <>
            <h2>Регистрация</h2>
            <form className='sign-up-form' onChange={validate}>

                <div className='field'>
                    <input type='text' id='form-firstname' name='firstname' />
                    <label forhtml='form-firstname'>Имя</label>
                </div>

                <div className='field'>
                    <input type='text' id='form-lastname' name='lastname' />
                    <label forhtml='form-lastname'>Фамилия</label>
                </div>

                <div className='field'>
                    <input type='email' id='form-email' name='email' />
                    <label forhtml='form-email'>Email</label>
                </div>

                <div className='field'>
                    <input type='password' id='form-password' name='password' />
                    <label forhtml='form-password'>Пароль</label>
                </div>

                <button className='form-submit-btn' onClick={sendData}>Создать</button>

                {/* {this.state.responseFetched && <p className='error-msg'>{this.state.serverMessage}</p>} */}
            </form>
        </>
    )
}
