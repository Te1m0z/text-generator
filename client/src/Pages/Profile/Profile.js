import React from 'react'
import './Profile.sass';

class Profile extends React.Component {

    texts = [];

    componentDidMount = async () => {

        let data = new FormData();
        data.append('id', this.props.user.data.id);

        await fetch('http://lib-text-generator/get-texts', {
            method: 'post',
            body: data,
        })
        .then(res => res.text())
        // .then(res => res.forEach(el => JSON.stringify(el)))
        .then(res => console.log(res))
    }

    render() {
        return (
            <>
                <h2>Профиль</h2>
                <main>
                    <h2>Логин: <span>{this.props.user.data.name}</span></h2>
                    <h2>Дата создания аккаунта: <span>{this.props.user.data.date}</span></h2>
                    <h2>ID аккаунта: <span>{this.props.user.data.id}</span></h2>

                    <ul>
                        {this.texts.map(item => <li>{JSON.parse(item)}</li>)}
                    </ul>
                </main>
            </>
        )
    }
}

export default Profile;
