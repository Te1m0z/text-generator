import React from 'react';
import './Login.sass';

export default function Login() {

    // sendData = async e => {

    //     e.preventDefault();
    //     let formData = new FormData(e.target.parentNode);

    //     if (!formData.get('login')) {
    //         document.getElementById('form-name').classList.remove('green');
    //         document.getElementById('form-name').classList.add('red');
    //     } else if (!formData.get('password')) {
    //         document.getElementById('form-pass').classList.remove('green');
    //         document.getElementById('form-pass').classList.add('red');
    //     } else {

    //         await fetch('http://lib-text-generator/api/login', {
    //             method: 'POST',
    //             body: formData,
    //         })
    //             .then(res => res.json())
    //             .then(res => {

    //                 if (res.authorized && !res.isError) {
    //                     this.setState({ serverMessage: res.serverMessage })
    //                 } else {
    //                     this.setState({
    //                         responseFetched: true,
    //                         error: res.isError,
    //                         serverMessage: res.serverMessage
    //                     })
    //                 }
    //             })
    //             .catch(error => console.log(error))
    //     }
    // }

    const validate = e => {
        if (e.target.value) {
            e.target.classList.remove('red');
            e.target.classList.add('green');
        } else {
            e.target.classList.remove('green', 'red');
        }
    }

    return (
        <>
            <h2>Вход</h2>
            <form className='login-up-form' onChange={validate}>

                <div className='field'>
                    <input type='text' id='form-name' name='login' />
                    <label forhtml='form-name'>Логин</label>
                </div>

                <div className='field'>
                    <input type='password' id='form-pass' name='password' />
                    <label forhtml='form-pass'>Пароль</label>
                </div>

                <button type='submit' className='form-submit-btn'>Войти</button>

                {/* {this.state.responseFetched && <p className='error-msg'>{this.state.serverMessage}</p>} */}
            </form>
        </>
    )
}
