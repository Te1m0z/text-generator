import React from 'react';
import Examples from '../../Components/Examples/Examples.js';
import FormDocument from '../../Components/FormDocument/FormDocument.js';
import ListReady from '../../Components/ListReady/ListReady.js';
import './home.sass';

export default function Home() {

    return (
        <main className='content'>
            <Examples />
            <ListReady />
            <div className='content__title'>Генератор Библиографических Списков</div>
            <FormDocument />
        </main>
    );
}
