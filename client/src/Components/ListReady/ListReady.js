import React from 'react';
import './list-ready.sass';

class ListReady extends React.Component {

    open = () => document.querySelector('.list-ready-window').classList.toggle('hide')

    render() {
        return (
            <div className={`list-ready-window hide`}>
                <h3>Получить созданный список</h3>
                <form action='http://text-gen/pdf-file.php' method='post' target='_blank'>
                    <textarea id='list-ready-text' name='ready-text' defaultValue={`\\begin{thebibliography}{9}\n\n\\end{thebibliography}`}></textarea>
                    <div style={{ display: 'flex' }}>
                        <button type='submit' className='form-submit-btn'>Открыть PDF</button>
                        <button type='submit' className='form-submit-btn'>Открыть LibTex</button>
                    </div>
                </form>
                <button type='button' className='list-ready-open-btn handle-btn' onClick={this.open}>
                    <span>Готовый список</span>
                </button>
                <button type='button' className='list-ready-close-btn handle-btn' onClick={this.open}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000">
                        <path d="M0 0h24v24H0V0z" fill="none" />
                        <path d="M14.59 8L12 10.59 9.41 8 8 9.41 10.59 12 8 14.59 9.41 16 12 13.41 14.59 16 16 14.59 13.41 12 16 9.41 14.59 8zM12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z" />
                    </svg>
                </button>
            </div>
        )
    }
}

export default ListReady;
