import React from 'react';
import {Link} from 'react-router-dom';
import './header.sass';

export default function Header(props) {

    return (
        <header className='header'>
            <div className='container header__container'>

                <div className='site-name'>
                    <Link to='/'>
                        <span>Библиографические списки</span>
                    </Link>
                </div>

                <nav className='lang-nav'>
                    <button>RU</button>
                    <button>EN</button>
                </nav>

                <nav className='auth-nav'>
                    <Link to='/login'>Войти</Link>
                    <Link to='/registration'>Зарегистрироваться</Link>
                </nav>

            </div>
        </header>
    )
}
