import React from 'react'

class Input extends React.Component {

    render() {
        return (
            <div className='field'>
                <textarea id={this.props.id} rows={this.props.rows} placeholder={this.props.placeholder}></textarea>
                <label htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        )
    }
}

export default Input;
