import React from 'react'

class Input extends React.Component {

    render() {
        return (
            <div className='field' style={this.props.style}>
                <input type={this.props.type} id={this.props.id} placeholder={this.props.placeholder} />
                <label htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        )
    }
}

export default Input;
