import React, { useEffect, useState } from 'react';
import Input from '../Input/Input';
import Textarea from '../Textarea/Textarea.js';
import Modal from '../Modal/Modal.js';

export default function BookTab(props) {

    const [openedModal, openModal] = useState(false);
    const [stroke, setStroke] = useState(null);

    const closeFunc = () => {
        openModal(false);
        document.querySelector('.modal__overlay').remove();
    };

    const runDoi = e => e.target.setAttribute('href', 'https://www.doi.org/' + document.getElementById('book-doi').value);

    const configure = () => {
        openModal(true);
        let
            doi = document.getElementById('book-doi').value,
            a = document.getElementById('book-author').value,
            n = document.getElementById('book-name').value,
            b_c_num = document.getElementById('book-curr-tome-number').value,
            b_max_num = document.getElementById('book-all-tome-number').value,
            b_c_name = document.getElementById('book-curr-tome-name').value,
            ad_s = document.getElementById('book-series').value,
            ad = document.getElementById('book-more-info').value,
            pl = document.getElementById('book-place').value,
            pb = document.getElementById('book-publish').value,
            py = document.getElementById('book-year').value,
            pg = document.getElementById('book-pages').value;

        let result_stroke = `${a} ${n}. ${pl} : ${pb}, ${py}. ${pg} с.`;

        // если есть серия
        if (ad_s) {
            result_stroke = `${a} ${n}. ${pl} : ${pb}, ${py}. ${pg} с. c. (${ad_s})`;
        }
        // если есть том
        if (b_c_num && b_max_num) {
            result_stroke = `${a} ${n} : в ${b_max_num} т. Т. ${b_c_num}. ${pl} : ${pb}, ${py}. ${pg} с.`;
        }
        // если есть название тома
        if (b_c_num && b_max_num && b_c_name) {
            result_stroke = `${a} ${n} : в ${b_max_num} т. Т. ${b_c_num} : ${b_c_name} ${pl} : ${pb}, ${py}. ${pg} с.`;
        }
        // если есть название тома и серия
        if (b_c_num && b_max_num && b_c_name && ad_s) {
            result_stroke = `${a} ${n} : в ${b_max_num} т. Т. ${b_c_num} : ${b_c_name} ${pl} : ${pb}, ${py}. ${pg} с. c. (${ad_s})`;
        }
        if (ad) {
            result_stroke = result_stroke + ' [' + ad + ']';
        }
        if (doi) {
            result_stroke = result_stroke + ' ' + 'https://www.doi.org/' + doi;
        }

        setStroke(result_stroke);
    }

    return (
        <div className={`book-tab form-tab ${props.display === 'book' ? 'd-block' : 'd-none'}`}>
            <Input type='text' id='book-doi' label='DOI номер' placeholder='10.18500/1816-9791-2019-19-3-317-325' />
            <span className='doi-label-2'>doi.org/</span>
            <a type='button' className='add-btn field' onClick={runDoi} target='_blank'>Проверить</a>
            <Textarea id='book-author' label='Автор(ы)' rows='3' placeholder='Иванов И. И, Петров Д. А.' />
            <Textarea id='book-name' label='Название' rows='3' />
            <Input type='text' id='book-curr-tome-number' label='Том' style={{ width: 60, display: 'inline-block', marginRight: 20 }} />
            <Input type='text' id='book-all-tome-number' label='Всего томов' style={{ width: 150, display: 'inline-block' }} />
            <Input type='text' id='book-curr-tome-name' label='Название тома' />

            <p className='field'>Дополнительные данные о книге</p>

            <section className='book-additional field'>
                <Input type='text' id='book-series' label='Серия' />
                <Textarea id='book-more-info' label='Прочее' rows='3' />
            </section>

            <Input type='text' id='book-place' label='Место издания (город)' placeholder='Саратов' />
            <Input type='text' id='book-publish' label='Издательство' placeholder='Просвещение' />
            <Input type='text' id='book-year' label='Год издания' placeholder='2010' />
            <Input type='text' id='book-pages' label='Количество страниц' placeholder='250' />
            <button type='button' className='form-submit-btn' onClick={configure}>Далее</button>

            {openedModal && <Modal close={closeFunc} stroke={stroke} />}
        </div>
    )
}
