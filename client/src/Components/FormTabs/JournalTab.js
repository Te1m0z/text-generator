import React from 'react';
import Input from '../Input/Input';

export default function JournalTab(props) {

    return (
        <div className={`journal-tab form-tab ${props.display === 'journal' ? 'd-block' : 'd-none'}`}>

            <Input type='text' id='journal-doi' label='DOI номер журнала' required={false} />
            <Input type='text' id='journal-author' label='Автор статьи' required={true} />
            <Input type='text' id='journal-name' label='Название статьи' required={true} />

            <div className='field'>
                <textarea id='journal-additional' rows='3'></textarea>
                <label htmlFor='journal-additional'>Доп. данные о статье</label>
            </div>

            <Input type='text' id='journal-place' label='Место издания (город) журнала' required={true} />
            <Input type='text' id='journal-publish' label='Издательство журнала' required={true} />
            <Input type='text' id='journal-pages' label='Количество страниц' required={true} />

            <button type='button' className='form-submit-btn'>Далее</button>

        </div>
    )
}
