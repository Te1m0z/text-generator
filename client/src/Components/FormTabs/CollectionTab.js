import React from 'react';
import Input from '../Input/Input';
import Modal from '../Modal/Modal.js';

export default function CollectionTab(props) {

    // constructor(p) {
    //     super(p);
    //     this.state = {
    //         modal: false 
    //     }
    // }

    // close = () => this.setState({ modal: false });

        return (
            <div className={`collection-tab form-tab ${props.display === 'collection' ? 'd-block' : 'd-none'}`}>

                <Input type='text' id='collection-doi' label='DOI номер сборника' required={false} />
                <Input type='text' id='collection-author' label='Автор сборника' required={true} />
                <Input type='text' id='collection-name' label='Название сборника' required={true} />

                <div className='field'>
                    <textarea id='collection-additional' rows='3'></textarea>
                    <label htmlFor='collection-additional'>Доп. данные о сборнике</label>
                </div>

                <Input type='text' id='collection-place' label='Место издания (город) сборника' required={true} />
                <Input type='text' id='collection-publish' label='Издательство сборника' required={true} />
                <Input type='text' id='collection-pages' label='Количество страниц сборника' required={true} />

                <button type='button' className='form-submit-btn' onClick={() => this.setState({modal: true})}>Далее</button>
                {/* { this.state.modal && <Modal handle={this.close} stroke='dada' /> } */}
            </div>
        )
}
