import React from 'react'
import Input from '../Input/Input';


export default function EResource(props) {


    return (
        <div className={`e-resource-tab form-tab ${props.display === 'e-resource' ? 'd-block' : 'd-none'}`}>

            <Input type='text' id='e-resource-doi' label='DOI номер' required={false} />
            <Input type='text' id='e-resource-author' label='Автор' required={true} />
            <Input type='text' id='e-resource-name' label='Название' required={true} />

            <div className='field'>
                <textarea id='e-resource-additional' rows='3'></textarea>
                <label htmlFor='e-resource-additional'>Доп. данные о книге</label>
            </div>

            <Input type='text' id='e-resource-place' label='Место издания (город)' required={true} />
            <Input type='text' id='e-resource-publish' label='Издательство' required={true} />
            <Input type='text' id='e-resource-pages' label='Количество страниц' required={true} />

            <button type='button' className='form-submit-btn'>Далее</button>

        </div>
    )
}
