import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './ModalWindow.sass'

export default function Modal(props) {

    let root = document.createElement('div');
    root.classList.add('modal__overlay');

    useEffect(() => document.body.appendChild(root), []);

    const closeWindow = e => {
        if (e.target.dataset.close) {
            document.querySelector('.modal').classList.remove('open');
            document.querySelector('.modal').classList.add('close');
            setTimeout(() => props.close(), 300);
        }
    }

    const copyData = () => {
        document.querySelector('.modal-textarea').select();
        document.execCommand('copy');
    }

    // sendData = async e => {
    //     // e.preventDefault();

    //     let data = new FormData();
    //     // data.append('Content-Type','text/plain; charset=UTF-8');
    //     data.append('id', this.props.user.data.id);
    //     data.append('data', JSON.stringify(this.props.object));


    //     await fetch('http://lib-text-generator/save-text', {
    //         method: 'POST',
    //         body: data,
    //     })
    //         .then(res => res.text())
    //         .then(res => console.log(res))
    //         .catch(err => console.log(err))
    // }

    console.log(props);

    return ReactDOM.createPortal(
        <div className='modal open' onClick={closeWindow} data-close="true">
            <div className='modal__body'>

                <div className="modal__body__header">
                    <h4 className="modal-title">Сделан библиографический список!</h4>
                </div>

                <div className="modal__body__content">
                    <button id="copy-stroke" type='button' onClick={copyData}>Скопировать</button>

                    <form action='http://lib-text-generator/pdf-file.php' method='post' target='_blank'>
                        <textarea id='list-ready-text' name='list' className="modal-textarea" defaultValue={props.stroke}></textarea>
                        <div style={{ display: 'flex' }}>
                            <button type='submit' className='form-submit-btn'>Открыть PDF</button>
                            <button type='submit' className='form-submit-btn'>Открыть LibTex</button>
                        </div>
                    </form>
                </div>

                <div className='modal__body__footer'>
                    {/* <button type='submit' onClick={() => this.props.user.authorized ? this.sendData() : console.log('вы должны быть авторизованы')}>Сохранить в личном кабинете</button> */}
                    <button type='button' data-close="true">Закрыть</button>
                </div>
            </div>
        </div>, root
    )
}

