import React, { useState, useEffect } from 'react';
import BookTab from '../FormTabs/BookTab.js';
import JournalTab from '../FormTabs/JournalTab.js';
import CollectionTab from '../FormTabs/CollectionTab.js';
import EResource from '../FormTabs/EResource.js';
import './FormDocument.sass';

export default function FormDocument() {

    const [doc, setDoc] = useState('book');

    useEffect(() => {
        document.querySelectorAll('.form-window-nav span').forEach(el => el.classList.remove('current'));
        document.getElementById(doc + '-link').classList.add('current');
    }, [doc]);

    return (
        <form id='document-form'>
            <nav className='form-window-nav'>
                <span id='book-link' onClick={() => setDoc('book')}>Книга</span>
                <span id='journal-link' onClick={() => setDoc('journal')}>Статья в журнале</span>
                <span id='collection-link' onClick={() => setDoc('collection')}>Статья в сборнике</span>
                <span id='e-resource-link' onClick={() => setDoc('e-resource')}>Электронный ресурс</span>
            </nav>

            <BookTab display={doc} />
            <JournalTab display={doc} />
            <CollectionTab display={doc} />
            <EResource display={doc} />
        </form>
    )
}
