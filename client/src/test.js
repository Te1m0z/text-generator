import { useRecoilState } from 'recoil';

export default function TextInput(props) {
    console.log(props)
    const [text, setText] = useRecoilState(props.textState);

    return (
        <div>
            <input type="text" value={text} onChange={e => setText(e.target.value)} />
            <br />dad
            Echo: {text}
        </div>
    );
}
